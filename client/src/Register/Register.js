import React from "react";

const url = "http://localhost:8000/admin/register";

export default class Register extends React.Component {

	state = {
		email: "",
		password: "",
		password2: "",
		error: ""
	}

	handleChange = (e) => {
		this.setState({[e.target.name] : e.target.value})
	}

	handleSubmit = e => {
        let { email , password , password2} = this.state
        e.preventDefault()
        fetch(url, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              email,
              password,
              password2
            }),
          })
        .then((response) => response.json())
              .then((responseJson) => {
                this.setState({error: responseJson.message})
                if( responseJson.ok ){
                   this.props.history.push("/admin/login")
                }
                  
        })
           .catch((e)=>{
            console.log({e})
        })
    }



	render () {
		return (
			<div>
				<h1>Register</h1>
				<form onSubmit = {this.handleSubmit}>
					<input 
						placeholder="email address" 
						onChange = {this.handleChange} 
						value = {this.state.email} 
						name = "email"/>
					<input 
						placeholder="password"
						onChange = {this.handleChange} 
						value = {this.state.password} 
						name = "password" />
					<input 
						placeholder="confirm password"
						onChange = {this.handleChange} 
						value = {this.state.password2} 
						name = "password2"/>
					<button>Submit</button>
				</form>
				<p>{this.state.error}</p>
			</div>
			)
	}
}