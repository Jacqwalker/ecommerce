import React from "react";
import AddProduct from "./AddProduct";
import ShowProducts from "./ShowProducts";
import Logout from "../Login/Logout";

export default class Dashboard extends React.Component {

	
	render () {
		return (
			<div>
				<Logout />
				<AddProduct />
				<ShowProducts />
			</div>)
	}
}