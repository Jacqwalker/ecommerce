import React from "react";

export default class DeleteProducts extends React.Component {
	state = {
		message: ""
	}

	handleDelete = () => {
		let {id} = this.props.id;

		fetch("http://localhost:8000/admin/products/delete", {
			method: "POST",
			headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            	id
            })
		}).then((response) => response.json())
		.then((responseJSON) => {
			if (responseJSON.ok) {
				this.setState({message: responseJSON.message});
			} 
		}).catch((e) => {
			this.setState({message: "something is not working, please try again"})
		})
	}

	render () {
		return (
			<div>
				<button onClick = {this.handleDelete}>Delete</button>
				<p>{this.state.message}</p>
			</div>)
	}
}