import React from "react";
import DeleteProducts from "./DeleteProducts";
import EditProducts from "./EditProducts";

export default class ShowProducts extends React.Component {

	state = {
		products: [],
		error: ""
	}

	componentWillMount () {
		let url = "http://localhost:8000/admin/products";

		fetch(url)
		.then((response) => response.json())
		.then((responseJson) => {
			let products = responseJson;
			console.log(products)
			this.setState({products: products});
		})
        .catch((error)=>{
     		 this.setState({error:'something went wrong'})
    	})
	}

	ShowProducts = () => {
		return this.state.products.map((product, index) => {
			return <tr key={index}>
						<td>{product.productName}</td>
						<td>£{product.price}</td>
						<td>{product.imgURL}</td>
						<td>{product.SKU}</td>
						<td>{product.quantity}</td>
						<td><DeleteProducts id={product._id}/></td>
						<td><EditProducts id={product._id} productName={product.productName} price = {product.price} imgURL = {product.imgURL} SKU = {product.SKU} quantity = {product.quantity}/></td>
					</tr>
		})

	}	

	render () {
		return (
			<div>
				<table>
					<thead>
						<tr>
							<th>Product name</th>
							<th>Price</th>
							<th>Image</th>
							<th>SKU</th>
							<th>Quantity</th>
							<th>Delete</th>
							<th>Update</th>
						</tr>
					</thead>

					<tbody>
						{this.ShowProducts()}
					</tbody>
				</table>
				
			</div>
			)
	}
}