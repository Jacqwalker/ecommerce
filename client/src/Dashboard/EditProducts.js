import React from "react";
import UpdateProduct from "./UpdateProduct";

export default class UpdateProducts extends React.Component {

	state = {
		message: "",
		isClicked: false
	}

	handleUpdate = () => {
		this.setState({isClicked: true})
	}

	render () {
		return (
			<div>
				<button onClick = {this.handleUpdate}>Edit</button>
				{ this.state.isClicked ? 
					<UpdateProduct productName = {this.props.productName} price = {this.props.price} imgURL={this.props.imgURL} SKU={this.props.SKU} id = {this.props.id} quantity = {this.props.quantity}/> 
				: null }
			</div>
			)
	}
}