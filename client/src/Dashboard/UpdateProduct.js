import React from "react";

export default class EditProduct extends React.Component {

	state = {
		productName: this.props.productName,
		price: this.props.price,
		imgURL: this.props.imgURL,
		SKU: this.props.SKU,
		id: this.props.id,
		quantity: this.props.quantity,
		message: ""
	}

	handleChange = (e) => {
		this.setState({[e.target.name] : e.target.value})
	}

	handleSubmit = (e) => {
		let { productName, price, imgURL, SKU, id, quantity } = this.state;
		e.preventDefault();

		let url = "http://localhost:8000/admin/products/update";

		fetch(url, {
			method: "POST",
			headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            	productName, 
            	price, 
            	imgURL, 
            	SKU, 
            	id,
            	quantity
            })
		}).then((response) => response.json())
		.then(responseJson => {
			if (responseJson.ok) {
				this.setState({message: responseJson.message})
			}
		}).catch((e) => {
			this.setState({message: "something is not working, please try again"})
		})

	}

	render () {
		return (
			<div>
				<form onSubmit = {this.handleSubmit}>
					<input 
						value = {this.state.productName}
						name = "productName"
						onChange = {this.handleChange}/>
					<input 
						value = {this.state.price}
						name = "price"
						onChange = {this.handleChange}/>
					<input 
						value = {this.state.imgURL}
						name = "imgURL"
						onChange = {this.handleChange}/>
					<input 
						value = {this.state.SKU}
						name = "SKU"
						onChange = {this.handleChange}/>
					<input 
						value = {this.state.quantity} 
						name = "quantity"
						onChange = {this.handleChange}/>
					<button>Submit</button>
				</form>
				<p>{this.state.message}</p>
			</div>
		)
			
	}
}