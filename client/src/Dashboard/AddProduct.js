import React from "react";

export default class AddProduct extends React.Component {

	state = {
		productName : "",
		price : "",
		imgURL : "",
		SKU : "",
		quantity: "",
		message: ""
	}

	handleChange = (e) => {
		this.setState({[e.target.name] : e.target.value});
	}

	handleSubmit = (e) => {
		let {productName, price, imgURL, SKU, quantity} = this.state;
		e.preventDefault();

		fetch("http://localhost:8000/admin/products/add", {
			method: "POST",
			headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            	productName,
            	price,
            	imgURL,
            	SKU,
            	quantity
            })
		}).then((response) => response.json())
		.then((responseJSON) => {
			if (responseJSON.ok) {
				this.setState({message: responseJSON.message});
			} else {
				this.setState({message: responseJSON.message})
			}

		}).catch((e) => {
			this.setState({message: "something is not working, please try again"})
		})
	}


	render () {
		return (
			<div>
				<h1>Hello I am the dashboard</h1>
				<form onSubmit={this.handleSubmit}>
					<input 
						placeholder = "product name" 
						value={this.state.productName} 
						name = "productName"
						onChange = {this.handleChange}/>
					<input 
						placeholder = "price" 
						value={this.state.price} 
						name = "price"
						onChange = {this.handleChange}/>
					<input 
						placeholder = "image url" 
						value={this.state.imgURL} 
						name = "imgURL"
						onChange = {this.handleChange}/>
					<input 
						placeholder = "SKU item" 
						value = {this.state.SKU} 
						name = "SKU"
						onChange = {this.handleChange}/>
					<input 
						placeholder = "Quantity" 
						value = {this.state.quantity} 
						name = "quantity"
						onChange = {this.handleChange}/>
					<button>Add product</button>
				</form>

				<p>{this.state.productName}</p>
				<p>{this.state.price}</p>
				<p>{this.state.imgURL}</p>
				<p>{this.state.SKU}</p>
				<p>{this.state.quantity}</p>
				<p>{this.state.message}</p>
			</div>)
	}
}