import React from "react";

export default class CartButton extends React.Component {
	state = {
		id: [],
		message: ""
	}

	handleClick = () => {
		
		// localStorage.setItem("quantity", this.state.count);
		// localStorage.setItem("price", this.state.price);
		let {id} = this.state;
		let productId = this.props.id;
		id.push(productId);
		this.setState({id}, () => console.log(this.state.id))
		// debugger;
	}

	render () {

		return (
			<div>
				<p>Quantity</p>
				<button onClick = {this.handleClick}>Add to cart</button>
				<p>{this.state.message}</p>
			</div>
			)
	}
}