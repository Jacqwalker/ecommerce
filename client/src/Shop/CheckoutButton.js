import React from "react";
import { withRouter } from 'react-router-dom';

class CheckoutButton extends React.Component {

	state = {
		orderTotal: this.props.total
	}
	
	handleClick = (e) => {
		let path = "/checkout"
		this.props.history.push(path)
	}

	render () {
		return (
			<button onClick = {this.handleClick}>Checkout Securely</button>
			)
	}
}

export default withRouter(CheckoutButton)