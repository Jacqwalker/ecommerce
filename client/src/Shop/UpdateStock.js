import React from "react";

export default class UpdateStock extends React.Component {

	state = {
		products: [],
		quantity: "",
		message: ""
	}

	componentDidMount () {
		let id = localStorage.getItem("id");
		console.log(id)
		
		let url = `http://localhost:8000/admin/products/checkQuantity/${id}`;

		fetch(url)
		.then((response) => response.json())
		.then((responseJson) => {
			let products = responseJson;
			console.log(products)
			this.setState({products: products});
    		this.updateQuantity()
		})
        .catch((error)=>{
     		 debugger
     		 this.setState({error:'something went wrong'})
    	})
	}

	updateQuantity = () => {
		let ordererdQuantity = localStorage.getItem("quantity");
		let inStockQuantity = this.state.products[0].quantity;
		let updatedQuantity = inStockQuantity  - ordererdQuantity;
		let id = localStorage.getItem("id");
		let { quantity } = this.state;

		let url = `http://localhost:8000/admin/products/updateQuantity`
		console.log(url);

		fetch(url, {
			method: "POST",
			headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
				updatedQuantity,
				id
            })
		}).then((response) => response.json())
		.then((responseJSON) => {
			console.log(responseJSON);
			this.setState({message: responseJSON.message})
			}).catch((e) => {
			this.setState({message: "something is not working, please try again"})
		})
	}

	render () {
		return (
			<div>
				{this.state.message}
			</div>
			)
	}
}