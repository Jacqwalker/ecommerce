import React from "react";

export default class QuantityButton extends React.Component {
	
	state = {
		quantity: this.props.quantity,
		id: this.props.id,
		message: ""
	}

	handleChange = (e) => {
		this.setState({[e.target.name] : e.target.value})
	}

	handleClick = () => {
		localStorage.setItem("id", this.state.id);
		localStorage.setItem("quantity", this.state.quantity);
		this.setState({message: "the shopping bag has been successfully updated"})
	}

	render () {
		return (
			<div>
				<p>Quantity</p>
				<input 
					value={this.state.quantity} 
					name="quantity"
					onChange = {this.handleChange}
					/>
				<button onClick = {this.handleClick}>Update item</button>
				<p>{this.state.message}</p>
			</div>
			)
	}

}