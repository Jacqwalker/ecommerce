import React from "react";
import CartButton from "./CartButton";

export default class Home extends React.Component {
	state = {
		products: [],
		message: ""
	}

	componentWillMount () {
		let url = "http://localhost:8000/admin/products";

		fetch(url)
		.then((response) => response.json())
		.then((responseJson) => {
			let products = responseJson;
			this.setState({products: products});
		})
        .catch((error)=>{
     		 debugger
     		 this.setState({error:'something went wrong'})
    	})
	}

	ShowProducts = () => {
		return this.state.products.map((product, index) => {
			return <div key={index}>
						<img src = {product.imgURL} alt={product.productName}/>	
						<h2>{product.productName}</h2>
						<h3>£{product.price}</h3>
						<CartButton id = {product._id} price={product.price}/>
					</div>
		})
	}	

	render () {
		return (
			<div>
				<h1>I am the home page</h1>
				<a href="/cart">My bag</a>
				<div>
					{this.ShowProducts()}
				</div>
			</div>)
	}
}