import React from "react";
import QuantityButton from "./QuantityButton";
import RemoveItemFromCart from "./RemoveItemFromCart";
import EmptyShoppingCart from "./EmptyShoppingCart";
import CheckoutButton from "./CheckoutButton";
import OrderTotal from "./OrderTotal"

export default class ShoppingCart extends React.Component {
	state = {
		products: [],
		orderTotal: ""
	}

	getOrderTotal = (total) => {
		this.setState({orderTotal: total})
	}

	componentDidMount () {
		let id = localStorage.getItem("id");
		console.log(id)
		
		let url = `http://localhost:8000/shoppingCart/${id}`

		fetch(url)
		.then((response) => response.json())
		.then((responseJson) => {
			let products = responseJson;
			console.log(products)
			this.setState({products: products});
		})
        .catch((error)=>{
     		 debugger
     		 this.setState({error:'something went wrong'})
    	})
	}

	showProductsInCart = () => {
		let quantity = localStorage.getItem("quantity");
		return (
			this.state.products.map((product, ind) => {
				return (
					<div key={ind}>
						<ul>
							<li><img src = {product.imgURL} alt = {product.productName}/></li>
							<li>{product.productName}</li>
							<li>{product.price}</li>
							<li><QuantityButton quantity = {quantity} id = {product._id}/></li>
							<li><RemoveItemFromCart /></li>
						</ul>
					</div>
					)
			})
			)
	}



	render () {
		let quantity = localStorage.getItem("quantity");
		return (
			<div>
				<h1>Shopping cart</h1>
				{ quantity > 0 ? 
					<div>
						{this.showProductsInCart()}
						<OrderTotal getTotal={this.getOrderTotal}/>
						<CheckoutButton total={this.state.orderTotal}/>
					</div> : 
				<div>
					<EmptyShoppingCart />
				</div> }
			</div>
		)
	}
}