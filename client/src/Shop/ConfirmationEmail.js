import React from "react";

const url = "http://localhost:8000/sendEmail"

export default class ConfirmationEmail extends React.Component {
	state = {
		message: "",
	}

	componentDidMount () {
		fetch(url, {
			method: "POST",
			headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
				email: "jackie@barcelonacodeschool.com",
				subject: "Your Walkr Boys clothes order",
				messageHeading: "Your order has been successfully placed",
				message: "Thanks for your order. It will be delivered to you in 3 to 5 working days"

            })
		}).then((response) => response.json())
		.then((responseJSON) => {
			console.log(responseJSON);
			this.setState({message: responseJSON.message})
			}).catch((e) => {
			this.setState({message: "something is not working, please try again"})
		})
	}

	render () {
		return (
			<div>
				<p>{this.state.message}</p>
			</div>
			)
	}
}