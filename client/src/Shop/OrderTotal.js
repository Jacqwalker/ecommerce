import React from "react";

export default class OrderTotal extends React.Component {

	state = {
		total: 0
	}
	
	componentDidMount () {
		let quantity = localStorage.getItem("quantity")
		let price = localStorage.getItem("price")
		let orderTotal;

		orderTotal = quantity * price;
		this.setState({total: orderTotal})
		this.props.getTotal(this.state.total)
	}

	render () {
		
		return (
			<div>
				<h1>Order Summary</h1>
				<p>Order value</p>
				<p>£{this.state.total}</p>
			</div>

			)
	}
}