import React from "react";

export default class EmptyShoppingCart extends React.Component {
	render () {
		return (
			<div>
				<h1>Your shopping basket is currently empty</h1>
				<button>Continue Shopping</button>
			</div>
			)
	}
}