import React from "react";

export default class RemoveItemFromCart extends React.Component {

	handleClick = () => {
		localStorage.clear();
	}

	render () {
		return (
			<div>
				<button onClick = {this.handleClick}>Delete Item</button>
			</div>
			)
	}

}