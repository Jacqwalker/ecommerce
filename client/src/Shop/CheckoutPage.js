import React from "react";
import StripeCheckout from 'react-stripe-checkout';
import ConfirmationEmail from "./ConfirmationEmail";

const STRIPE_PUBLISHABLE = 'pk_test_NwLv8MWzr7adYyAcehJ8xvxB00my2At0o5';
const url = "http://localhost:8000/payment";
const currency = "GBP";

export default class CheckoutPage extends React.Component {
	state = {
		message: "",
		paymentSuccess: false
	}

	onToken = (token) => {
		fetch(url, {
			method: "POST",
			headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
				amount: 150,
				source: token.id,
				currency
            })
		}).then((response) => response.json())
		.then((responseJSON) => {
			console.log(responseJSON);
			this.setState({paymentSuccess: true})
			}).catch((e) => {
			this.setState({message: "something is not working, please try again"})
		})
		console.log(token)
	}

	render () {
		let {paymentSuccess} = this.state;

		return (
			<div>
				<h1>I am the Checkout Page</h1>
				<StripeCheckout
				stripeKey={STRIPE_PUBLISHABLE}
				token={this.onToken}
				description={"WALKR - the best in boys clothing"}
				amount={150}
				label="Pay now" />
				{paymentSuccess ? 
					<div>
						<ConfirmationEmail />
					
					</div>
					: this.state.message }
			</div>

			
			)
	}
}