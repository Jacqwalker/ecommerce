import React from "react";

const url = "http://localhost:8000/admin/login";

export default class LogIn extends React.Component {
	
	state = {
		email: "",
		password: "",
		error: ""
	}

	handleChange = (e) => {
		this.setState({[e.target.name] : e.target.value})
	}

	handleSubmit = (e) => {
		let {email, password} = this.state;
		e.preventDefault();

		fetch(url, {
			method: "POST",
			headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
            	email, 
            	password
            }),
		}).then((response) => response.json())
		.then((responseJson) => {
			if (responseJson.ok) {
				localStorage.setItem('authToken',JSON.stringify(responseJson.token));
				this.props.history.push("/admin/dashboard")
			} else {
				this.setState({error: responseJson.message});
			}
		}).catch((e)=>{
            console.log("something is not working")
        })
	}

	render () {
		return (
			<div>
				<form onSubmit = {this.handleSubmit}>
					<input 
						placeholder="email"
						value = {this.state.email}
						name = "email"
						onChange = {this.handleChange}/>
					<input 
						placeholder = "password"
						value = {this.state.password}
						name = "password"
						onChange = {this.handleChange}/>
					<button>Submit</button>
				</form>
				<p>{this.state.error}</p>
			</div>	

		)
	}
}