import React from "react";

export default class Logout extends React.Component {
	
	handleClick = () => {
		localStorage.clear();
		
	}

	render () {
		return (
			<div>
				<button onClick={this.handleClick}>Log out</button>
			</div>
			)
	}
}