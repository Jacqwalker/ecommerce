import React from 'react';
import {BrowserRouter as Router, Route } from "react-router-dom";
import Register from "./Register/Register";
import LogIn from "./Login/LogIn";
import Dashboard from "./Dashboard/Dashboard";
import Home from "./Shop/Home";
import ShoppingCart from "./Shop/ShoppingCart";
import CheckoutPage from "./Shop/CheckoutPage";
import UpdateStock from "./Shop/UpdateStock";

import SecretComp from "./SecretComp.js";

function App() {
  return (
    <Router>
      <div>
        <Route path="/admin/register" component={Register}/>
        <Route path="/admin/login" component={LogIn}/>
        <Route path="/admin/dashboard" component={Dashboard}/>
        <Route exact path="/" component={Home}/>
        <Route path="/cart" component={ShoppingCart}/>
        <Route path="/checkout" component={CheckoutPage}/>
        <Route path="/stock" component={UpdateStock}/>
        
      </div>
    </Router>
    
  );
}

export default App;
