const adminUser = require("../models/User.Models");
const bcrypt = require("bcrypt");
const secret = require("../config");
const jwt    = require('jsonwebtoken');
const config     = require('../config');

class UserControllers {
	async _registerAdmin (req, res) {
		let {email, password, password2} = req.body;

		if (!email || !password || !password2) { 
			res.status(422).send({message: "all fields are required"})
		}

		if (password !== password2) return res.send({message: "the passwords do not match"})

		try {
			const user = await adminUser.findOne({email: email});
			if (user) return res.send({message: "Email address already exists"});

			const hash = await bcrypt.hash(password, 10);
			console.log("hash = ", hash);

			const newUser = {
				email, 
				password: hash
			}

			const create = await adminUser.create(newUser);
			res.send({ok: true, message: "user successfully added"})

		}

		catch (error) {
			res.send({error})
		}
	}

	async _logInAdmin (req, res) {
		let {email, password} = req.body;

		if (!email || !password) return res.send({message: "All fields are required"})
			
		try {
			const user = await adminUser.findOne({email});
			
			if (!user) return res.send({message: "email address does not exist"})

			const match = await bcrypt.compare(password, user.password);

			if (match) {
				const token = jwt.sign(user.toJSON(), config.secret, {expiresIn: 100080});
				res.send({ok: true, token, email})
				
			} else {
				res.send({message: "password is incorrect"})
			}
		}

		catch (error) {
			res.send({error})
		}
	}

	async _verifyToken (req, res) {
		try {
			const {token} = req.params;
			const decoded = jwt.verify(token, secret.config);
			res.send({message: "secret page"});
		}

		catch (e) {
			res.send({e})
		}
	}


	async _showAdminUsers (req, res) {
		try {
			const user = await adminUser.find({});
			res.send(user);	
		}

		catch (error) {
			res.send({error})
		}
		
	}

}



module.exports = new UserControllers ();
