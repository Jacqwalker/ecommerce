const nodemailer = require("nodemailer");
const pwd = require('../p').pwd;

const transport = nodemailer.createTransport({
			service: 'Gmail',
			auth: {
				user: 'jackie@barcelonacodeschool.com',
				pass: pwd,
			},
	});

class emailControllers {
	async _sendEmail (req, res) {
		try {
			const { email, subject, messageHeading, message } = req.body;

			const mailOptions = {
		    to: email,
		    subject: subject,
		    // composing body of the email
		    html: '<p>'+ messageHeading + '</p><p><pre>' + message + '</pre></p>'
			};
			transport.sendMail(mailOptions, (error, info) => {
				if (error) {
					console.log(error);
					return res.send(error)
			        // return res.redirect('/contacts')
			    }
			    console.log(`Message sent: ${info.response}`);
			    // res.redirect('/contacts')
			    return res.send({message: "Your order is complete and we have sent you an email confirming the order"})
			});
		}

		catch (e) {
			res.status(500).send({e})
		}

	}
}

module.exports = new emailControllers();