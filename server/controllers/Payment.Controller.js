const stripe = require("stripe")("sk_test_qFGmKsB1LhhcSF1ekMV3Eb9m00n3AsVl15");

class PaymentControllers {
	async _payment (req, res) {
		try {
			const result = await stripe.charges.create(req.body);
			res.status(200).send({result})
		}

		catch (e) {
			res.status(500).send({e})
		}

	}
}

module.exports = new PaymentControllers();