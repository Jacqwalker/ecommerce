const Products = require("../models/Products.Models");;

class ProductControllers {
	async _addProduct (req, res) {
		let {productName, price, imgURL, SKU, quantity} = req.body;
		
		if (!productName || !price || !imgURL || !SKU || !quantity) return res.send({message: "all fields are required"})

		const checkProductName = await Products.findOne({productName});
		if (checkProductName) return res.send({ok: false, message: "this product title already exists"})
			
		const checkImgURL = await Products.findOne({imgURL});
		if (checkImgURL) return res.send({ok: false, message: "this url is associated with another product, please amend"})

		try {
			const newProduct = {
				productName,
				price, 
				imgURL,
				SKU,
				quantity
			}
			
			const create = await Products.create(newProduct);
			res.send({ok: true, message: "Product successfully added to database"});
		}

		catch (e) {
			res.send({message: e})
		}
	}

	async _deleteProduct (req, res) {
		try {
			const {id} = req.body;
			const product = await Products.deleteOne({id});
			res.send({ok: true, message: "Product was successfully deleted from the database"})
		}

		catch (error) {
			res.send({error})
		}
	}

	async _updateProduct (req, res) {
		try {
			const { productName, price, imgURL, SKU, id, quantity } = req.body;
			const product = await Products.updateOne({"_id": id}, {$set: {"productName": productName, "price": price, "imgURL": imgURL, "SKU": SKU, "quantity": quantity} });
			res.send({ok: true, message: "This product has been updated in the database"})
		}

		catch (e) {
			res.send({e})
		}
	}

	async _shoppingCartProducts (req, res) {
		try {
			const { id } = req.params;
			let _id = id;
			const product = await Products.find({_id});
			res.send(product)
		}

		catch (e) {
			res.send({e})
		}
	}

	async _checkQuantity (req, res) {
		try {
			const { id } = req.params;
			let _id = id;
			const product = await Products.find({_id});
			res.send(product)
		}

		catch (e) {
			res.send({e})
		}
	}

	async _updateQuantity (req, res) {
		try {
			const { id, updatedQuantity } = req.body;
			const product = await Products.UpdateOne({"_id": id}, {$set: {"quantity": updatedQuantity} });
			res.send({ok: true, message: "This stock quantity has been updated in the database"})
		}

		catch (e) {
			res.send({message: "this is not working"})
		}
	}

	async _showAllProducts (req, res) {
		try {
			const product = await Products.find({});
			res.send(product)
		} 

		catch (e) {
			res.send({e})
		}
	}

}

module.exports = new ProductControllers();