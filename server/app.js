const express = require("express"),
		app	  = express(),
		userRoutes = require("./routes/User.Routes"),
		productRoutes = require("./routes/Products.Routes"),
		paymentRoutes = require("./routes/Payment.Routes"),
		emailRoutes = require("./routes/Email.Route.js"),
		cors = require("cors");

const bodyParser= require('body-parser')
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

const mongoose = require("mongoose");
mongoose.connect("mongodb://127.0.0.1/ecommerce_db", () => {
	console.log("I am connected to mongo db")
});

app.use(cors());

// admin login, register routes
app.use("/", userRoutes);
// product dashboard routes
app.use("/", productRoutes);
// payment route
app.use("/", paymentRoutes);
// email route
app.use("/", emailRoutes);

app.listen(8000, () => {
	console.log("i am running on port 8000")
})