const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductsSchema = new Schema({
	productName : {
		type: String,
		unique: true
	},
	price : {
		type: Number,
	},
	imgURL : {
		type: String,
		unique: true
	},
	SKU : {
		type: String,
	},
	quantity : {
		type: Number
	}
});


module.exports = mongoose.model("products", ProductsSchema);