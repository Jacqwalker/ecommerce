const express = require("express"),
		app	  = express(),
	   router = express.Router();
const controller = require("../controllers/User.Controllers");

// Register and Login admin
router.post("/admin/register", controller._registerAdmin);
router.get("/admin", controller._showAdminUsers);
router.post("/admin/login", controller._logInAdmin);
router.get("/verifyToken/:token", controller._verifyToken);


module.exports = router;