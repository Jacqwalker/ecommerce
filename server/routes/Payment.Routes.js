const express = require("express"),
		app	  = express(),
	   router = express.Router();
const paymentController = require("../controllers/Payment.Controller");


router.post("/payment", paymentController._payment);

module.exports = router;