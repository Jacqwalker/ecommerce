const express = require("express"),
		app	  = express(),
	   router = express.Router();
const productsController = require("../controllers/Product.Controllers");

// Products dashboard
router.post("/admin/products/add", productsController._addProduct);
router.post("/admin/products/delete", productsController._deleteProduct);
router.post("/admin/products/update", productsController._updateProduct);

// Update to quantity after order is completed
router.get("/admin/products/checkQuantity/:id", productsController._checkQuantity);
router.post("/admin/products/updateQuantity", productsController._updateQuantity);

// Shopping Cart
router.get("/shoppingCart/:id", productsController._shoppingCartProducts);


router.get("/admin/products", productsController._showAllProducts);


module.exports = router;